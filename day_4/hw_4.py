from datetime import datetime
from typing import List, Optional


class Author:
    def __init__(self, first_name: str, last_name: str, year_of_birth: int):
        """
        Represents an author with first name, last name, and year of birth.

        Parameters:
        - first_name (str): The first name of the author.
        - last_name (str): The last name of the author.
        - year_of_birth (int): The year of birth of the author.
        """
        self.first_name = first_name
        self.last_name = last_name
        self.year_of_birth = year_of_birth

    def __str__(self) -> str:
        return f"{self.first_name} {self.last_name} (Born: {self.year_of_birth})"

    def __repr__(self) -> str:
        return f"Author('{self.first_name}', '{self.last_name}', {self.year_of_birth})"

    def __eq__(self, other) -> bool:
        if isinstance(other, Author):
            return (
                self.first_name == other.first_name
                and self.last_name == other.last_name
                and self.year_of_birth == other.year_of_birth
            )
        return False


class Genre:
    def __init__(self, name: str, description: str):
        """
        Represents a genre with a name and description.

        Parameters:
        - name (str): The name of the genre.
        - description (str): The description of the genre.
        """
        self.name = name
        self.description = description

    def __str__(self) -> str:
        return f"{self.name} - {self.description}"

    def __repr__(self) -> str:
        return f"Genre('{self.name}', '{self.description}')"


class Book:
    def __init__(
        self,
        title: str,
        language: str,
        year_of_publication: int,
        *authors: Author,
        description: Optional[str] = None,
        isbn: Optional[str] = None,
        genres: Optional[List[Genre]] = None,
    ):
        """
        Represents a book with title, language, authors, year of publication, and optional description, ISBN, and
        genres.

        Parameters:
        - title (str): The title of the book.
        - language (str): The language of the book.
        - year_of_publication (int): The year of publication of the book.
        - *authors (Author): Variable number of authors (positional arguments).
        - description (str, optional): The description of the book.
        - isbn (str, optional): The ISBN of the book.
        - genres (List[Genre], optional): The list of genres to which the book belongs.
        """
        self.title = title
        self.language = language
        self.year_of_publication = year_of_publication
        self.authors = authors
        self.description = description
        self.isbn = isbn
        self.genres = genres

    def __str__(self) -> str:
        authors_str = ", ".join(str(author) for author in self.authors)
        genres_str = (
            ", ".join(str(genre) for genre in self.genres)
            if self.genres
            else "Not available"
        )

        return (
            f"Title: {self.title}\n"
            f"Language: {self.language}\n"
            f"Authors: {authors_str}\n"
            f"Year of Publication: {self.year_of_publication}\n"
            f"Description: {self.description}\n"
            f"ISBN: {self.isbn}\n"
            f"Genres: {genres_str}"
        )

    def __repr__(self) -> str:
        return (
            f"Book('{self.title}', '{self.language}', {self.year_of_publication}, "
            f"{', '.join(repr(author) for author in self.authors)}, "
            f"description='{self.description}', isbn='{self.isbn}', "
            f"genres={repr(self.genres) if self.genres else 'None'})"
        )

    def __eq__(self, other) -> bool:
        if isinstance(other, Book):
            return self.title == other.title and all(
                a1 == a2 for a1, a2 in zip(self.authors, other.authors)
            )
        return False

    def age_in_years(self) -> int:
        """
        Calculate the age of the book in years relative to the current year.

        Returns:
        - int: The age of the book in years.
        """
        current_year = datetime.now().year
        return current_year - self.year_of_publication
