from typing import Dict, Union


def calculate_character_probabilities(input_string: str) -> Dict[str, float]:
    """
    Calculate the probabilities of each character in the input string.

    Parameters:
    - input_string (str): The input string.

    Returns:
    - Dict[str, float]: A dictionary where keys are characters, and values are the probabilities of encountering each
    character.
    """
    char_count = {}
    total_chars = len(input_string)

    for char in input_string:
        char_count[char] = char_count.get(char, 0) + 1

    probabilities = {char: count / total_chars for char, count in char_count.items()}
    return probabilities


def compare_numbers(
    x: Union[int, float], y: Union[int, float]
) -> Union[int, float, str]:
    """
    Compare two numbers and determine the result based on the following logic:

    - If x is equal to 0 and y is equal to 0, return "game over".
    - If x is less than y, return the sum of x and y.
    - If x is equal to y, return 0.
    - If x is greater than y, return the difference between x and y.

    Parameters:
    - x (Union[int, float]): The first number for comparison.
    - y (Union[int, float]): The second number for comparison.

    Returns:
    - Union[int, float, str]: The result based on the specified logic.
    """
    result = (
        "game over" if x == 0 and y == 0 else x + y if x < y else 0 if x == y else x - y
    )
    return result


if __name__ == "__main__":
    input_str = "hello world"
    result = calculate_character_probabilities(input_str)
    print(result)

    x = 5.5
    y = 10
    result = compare_numbers(x, y)
    print(f"The result for x={x} and y={y} is: {result}")

    x = 10
    y = 10
    result = compare_numbers(x, y)
    print(f"The result for x={x} and y={y} is: {result}")

    x = 15
    y = 10.5
    result = compare_numbers(x, y)
    print(f"The result for x={x} and y={y} is: {result}")

    x = 0
    y = 0
    result = compare_numbers(x, y)
    print(f"The result for x={x} and y={y} is: {result}")
