from typing import List


class Author:
    def __init__(self, first_name: str, last_name: str, year_of_birth: int):
        """
        Represents an author with first name, last name, and year of birth.

        Parameters:
        - first_name (str): The first name of the author.
        - last_name (str): The last name of the author.
        - year_of_birth (int): The year of birth of the author.
        """
        self.first_name = first_name
        self.last_name = last_name
        self.year_of_birth = year_of_birth

    def __str__(self) -> str:
        return f"{self.first_name} {self.last_name} (Born: {self.year_of_birth})"

    def __repr__(self) -> str:
        return f"Author('{self.first_name}', '{self.last_name}', {self.year_of_birth})"

    def __eq__(self, other) -> bool:
        if isinstance(other, Author):
            return (
                self.first_name == other.first_name
                and self.last_name == other.last_name
                and self.year_of_birth == other.year_of_birth
            )
        return False


class Genre:
    def __init__(self, name: str, description: str):
        """
        Represents a genre with a name and description.

        Parameters:
        - name (str): The name of the genre.
        - description (str): The description of the genre.
        """
        self.name = name
        self.description = description

    def __str__(self) -> str:
        return f"{self.name} - {self.description}"

    def __repr__(self) -> str:
        return f"Genre('{self.name}', '{self.description}')"


class Book:
    def __init__(
        self,
        title: str,
        description: str,
        language: str,
        authors: List[Author],
        genres: List[Genre],
        year_of_publication: int,
        isbn: str,
    ):
        """
        Represents a book with title, description, language, authors, genres, year of publication, and ISBN.

        Parameters:
        - title (str): The title of the book.
        - description (str): The description of the book.
        - language (str): The language of the book.
        - authors (List[Author]): The list of authors of the book.
        - genres (List[Genre]): The list of genres to which the book belongs.
        - year_of_publication (int): The year of publication of the book.
        - isbn (str): The ISBN of the book.
        """
        self.title = title
        self.description = description
        self.language = language
        self.authors = authors
        self.genres = genres
        self.year_of_publication = year_of_publication
        self.isbn = isbn

    def __str__(self) -> str:
        authors_str = ", ".join(str(author) for author in self.authors)
        genres_str = ", ".join(str(genre) for genre in self.genres)

        return (
            f"Title: {self.title}\n"
            f"Description: {self.description}\n"
            f"Language: {self.language}\n"
            f"Authors: {authors_str}\n"
            f"Genres: {genres_str}\n"
            f"Year of Publication: {self.year_of_publication}\n"
            f"ISBN: {self.isbn}"
        )

    def __repr__(self) -> str:
        return (
            f"Book('{self.title}', '{self.description}', '{self.language}', {repr(self.authors)}, "
            f"{repr(self.genres)}, {self.year_of_publication}, '{self.isbn}')"
        )

    def __eq__(self, other) -> bool:
        if isinstance(other, Book):
            return self.title == other.title and self.authors == other.authors
        return False
